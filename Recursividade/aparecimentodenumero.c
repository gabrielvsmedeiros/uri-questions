#include <stdio.h>

/* 
Escreva uma função recursiva que 
determina quantas vezes um dígito k
ocorre em um número natural N. Por 
exemplo, o dígito 2 ocorre 3 vezes 
em 736428652
*/

int numeroDeDigitos(int n, int k) {
	if (n > 0) {
		if (n%10 == k) return numeroDeDigitos(n/10, k)+1;
		else return numeroDeDigitos(n/10, k)+0;
	}
	return 0;
}

int main() {
	int n, k;
	scanf("%d %d", &n, &k);

	printf("%d\n", numeroDeDigitos(n, k));
	return 0;
}
