#include <stdio.h>


int tetranacci(int n) {
  if (n == 1) return 0;
  else if (n == 2 || n == 3 || n == 4) return 1;
	else return tetranacci(n-1) + tetranacci(n-4) + tetranacci(n-3) + tetranacci(n-4);
}

int fibonacci(int num) {
  if (num == 1) return 0;
  else if (num == 2) return 1;
  else return fibonacci(num-1) + fibonacci(num-2);
}

int main() {
  int n;
  scanf("%d", &n);
  printf("O enésimo número da sequência eh: %d\n", tetranacci(n));
return 0;
}
