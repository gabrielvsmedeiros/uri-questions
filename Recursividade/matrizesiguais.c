#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	int **matrizA, **matrizB, i, j;
	int m, n; // VARIÁVEIS DOS TAMANHOS DAS MATRIZES
	char igual = 'T';

	scanf("%d %d", &m, &n);
	// ALOCAÇÃO DA MATRIZ A
	matrizA = (int **)calloc(m, sizeof(int*));
	if (matrizA == NULL) return 1;
	for (i = 0; i < m; i++)	{
		matrizA[i] = (int *)calloc(n, sizeof(int));
		if (matrizA[i] == NULL) return 1;
	}
	// ALOCAÇÃO DA MATRIZ B
	matrizB = (int **)calloc(m, sizeof(int*));
	if (matrizB == NULL) return 1;
	for (i = 0; i < m; i++) {
		matrizB[i] = (int *)calloc(n, sizeof(int));
		if (matrizB[i] == NULL) return 1;
	}

	// RECEBENDO VALORES DE A
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			scanf("%d", &matrizA[i][j]);
		}
	}

	// RECEBENDO VALORES DE B
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			scanf("%d", &matrizB[i][j]);
		}
	}

	// COMPARANDO AS DUAS MATRIZES
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			if (matrizA[i][j] != matrizB[i][j]) igual = 'F';
		}
	}


	if (igual == 'T') printf("São iguais!\n");
	else printf("São Diferentes!\n");



	return 0;
}