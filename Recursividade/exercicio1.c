#include <stdio.h>

/* 
Escreva uma função recursiva que 
determina quantas vezes um dígito k
ocorre em um número natural N. Por 
exemplo, o dígito 2 ocorre 3 vezes 
em 736428652
*/

int numeroDeDigitos(int valor, int digito) {
	if (valor < 0)
	{
		if (valor%10 == digito)
		{
			return numeroDeDigitos(valor/10, digito)+1;
		}
	} 
	else return 0;

}

int main() {
	int numero, digito;
	scanf("%d %d", &numero, &digito);

	printf("%d\n", numeroDeDigitos(numero, digito));
	return 0;
}
