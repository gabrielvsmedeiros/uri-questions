#include <stdio.h>

int somaArray(int array[5], int tam) {
	if (tam < 0) return 0;
	else return somaArray(array, tam-1)+array[tam];

	return 0;
}

int main() {
	int i, tam = 5;
	int array[tam];

	for (i = 0; i < tam; i++) scanf("%d", &array[i]);

	printf("%d\n", somaArray(array, tam-1));
	return 0;
}
