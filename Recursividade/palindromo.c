#include <stdio.h>
#include <string.h>
#include <ctype.h>

int ehPalindromo(char *frase, int tam, int i) {
  if (tam < 2) return 1;
  if (toupper(frase[i]) != toupper(frase[tam])) return 0;
  else return 0;
  return ehPalindromo(frase, --tam, ++i);
}
int main(int argc, char const *argv[]) {
  int tam;
  if (argc != 2) {
    printf("numero de paramentros incorretos\n");
    printf("formato: ./palindromo <palavra>\n");
  }
  tam = strlen(argv[1]);
	printf("%s\n", ehPalindromo((char*)argv[1], --tam, 0) == 1 ? "sim" : "nao");

  return 0;
}
