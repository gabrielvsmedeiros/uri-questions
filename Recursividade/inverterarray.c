#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int* inverterArray(int *array, int tam, int i) {
	int aux = 0;
	if (i >= tam) return array;
	else {
		aux = array[i];
		array[i] = array[tam];
		array[tam] = aux;

		inverterArray(array, tam-1, i+1);
	}
}

int main() {
	int i, tam, *array, *arrayInvertido;
	
	scanf("%d", &tam);

	array = (int *)calloc(tam,sizeof(int)); // ALOCANDO O VETOR 1
	if (array == NULL) return 1; // VERIFICA SE O ESPAÇO DE MEMÓRIA FOI ENCONTRADO

	srand(time(0));
	for (i = 0; i < tam; i++) array[i] = rand() % 10 + 1; 
	printf("Array Populado\n");
	for (i = 0; i < tam; i++) printf("%i ", array[i]);

	printf("\nArray Invertido\n");
	arrayInvertido = inverterArray(array, tam-1, i = 0);
	for (i = 0; i < tam; i++) printf("%d ", arrayInvertido[i]);
	printf("\n");
	return 0;
}



// PASSAGEM POR REFERÊNCIA

/*void inverterArray(int array[], int tam, int i) {
	int aux;
	if (i >= tam) return;
	else {
		aux = array[i];
		array[i] = array[tam];
		array[tam] = aux;

		inverterArray(array, tam-1, i+1);
	}
}

int main() {
	int i, tam, *array;
	
	scanf("%d", &tam);

	array = (int *)calloc(tam,sizeof(int)); // ALOCANDO O VETOR 1
	if (array == NULL) return 1; // VERIFICA SE O ESPAÇO DE MEMÓRIA FOI ENCONTRADO

	srand(time(0));
	for (i = 0; i < tam; i++) array[i] = rand() % 10 + 1; 
	printf("Array Populado\n");
	for (i = 0; i < tam; i++) printf("%i ", array[i]);

	printf("\nArray Invertido\n");
	inverterArray(array, tam-1, 0);
	for (i = 0; i < tam; i++) printf("%i ", array[i]);
	printf("\n");

	return 0;
}*/

