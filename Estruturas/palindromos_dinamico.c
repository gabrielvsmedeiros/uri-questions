#include <stdio.h>
#include <string.h>

void ehpalindromo(char *palavra, int index, int tam, int* resposta){
	if(index > tam/2){
		return;
	}
	else{
		if(palavra[index] != palavra[tam-1-index]){
			*resposta = 0;
		}
		ehpalindromo(palavra, index+1, tam, resposta);
	}
}

int main(int argc, char const *argv[])
{
	
	int resposta=1;

	printf("%s %lu\n", (char *)argv[1], strlen(argv[1]));
	ehpalindromo((char *)argv[1], 0, strlen(argv[1]), &resposta);
	if(resposta){
		printf("Eh palindromo\n");
	}
	else{
		printf("NAO eh palindromo\n");
	}
	return 0;
}