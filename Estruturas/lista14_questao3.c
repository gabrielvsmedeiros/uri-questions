#include <stdio.h>
#include <stdlib.h>

int main() {
	int i, j;

	typedef struct {
		char nome[50];
		char curso[20];
		int matricula;
	} Aluno;

	Aluno alunos[3];

	for (i = 0; i < 3; i++) {
		scanf("%s", alunos[i].nome);
		scanf("%s", alunos[i].curso);
		scanf("%i", &alunos[i].matricula);
	}

	printf("\nDados dos alunos que você acabou de cadastrar: \n");

	for (i = 0; i < 3; i++) {
		printf("\nAluno %i\n", i+1);

		printf("%s\n", alunos[i].nome);
		printf("%s\n", alunos[i].curso);
		printf("%i\n", alunos[i].matricula);
	}

	return 0;
}