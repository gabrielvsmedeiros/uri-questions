#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	int dia;
	int mes;
	int ano;
} data;

data leData();

void imprimeData(data atual);

int main() {
	imprimeData(leData());
}

data leData(){
	int dia, mes, ano;
	data dataLida;
	printf("Digite o dia: ");
	scanf("%d", &(dataLida.dia));
	printf("Digite o mes: ");
	scanf("%d", &mes);
	printf("Digite o ano: ");
	scanf("%d", &ano);

	dataLida.mes = mes;
	dataLida.ano = ano;
	
	return dataLida;
	
}

void imprimeData(data atual){
	printf("A data informada eh %d/%d/%d.\n", atual.dia, atual.mes, atual.ano);
}