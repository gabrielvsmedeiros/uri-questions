#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	int matricula;
	char nome[51];
	endereco endereco;
} cliente;

typedef struct {
	char logradouro[101];
	int numero;
	char bairro[51];
	long int cep;
} endereco;
