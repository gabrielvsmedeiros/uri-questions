#include <stdio.h>

int main(int argc, char const *argv[])
{
	int n, i;
	int vetFib[50];
	scanf("%d", &n);

	for(i=0;i<=n;i++){
		if(i<=1){
			vetFib[i] = i;
		}
		else{
			vetFib[i] = vetFib[i-1]+vetFib[i-2];
		}
	}
	printf("Fib(%d) = %d\n", n, vetFib[n]);

	return 0;
}