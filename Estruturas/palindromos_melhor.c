#include <stdio.h>
#include <string.h>
#include <ctype.h>

void ehpalindromo(char *palavra, int index, int tam, int* resposta){
	if(index > tam/2){
		return;
	}
	else{
		if(toupper(palavra[index]) != toupper(palavra[tam-1-index])){
			*resposta = 0;
		}
		ehpalindromo(palavra, index+1, tam, resposta);
	}
}

int main(int argc, char const *argv[])
{
	char palavra[20];
	int resposta=1;

	scanf("%s", palavra);
	printf("%s %lu\n", palavra, strlen(palavra));
	ehpalindromo(palavra, 0, strlen(palavra), &resposta);
	if(resposta){
		printf("Eh palindromo\n");
	}
	else{
		printf("NAO eh palindromo\n");
	}
	return 0;
}