#include <stdio.h>
#include <stdlib.h>

int main() {
	int i, j;

	char maiorPrimeiraNota;
	char maiorMediaGeral;
	char menorMediaGeral;

	int primeiraNota = 0;
	int mediaGeral = 0;

	typedef struct {
		int matricula;
		char nome[50];
		int n1;
		int n2;
		int n3;
	} Aluno;

	Aluno alunos[3];

	for (i = 0; i < 3; i++) {
		printf("Matricula: \n");
		scanf("%i", &alunos[i].matricula);
		printf("Nome do Aluno: \n");
		scanf("%s", alunos[i].nome);
		printf("Nota 1: \n");
		scanf("%i", &alunos[i].n1);
		printf("Nota 2: \n");
		scanf("%i", &alunos[i].n2);
		printf("Nota 3: \n");
		scanf("%i", &alunos[i].n3);
	}

	printf("\nDados requeridos pela questão\n");

	for (i = 0; i < 3; i++) {
		if (alunos[i].n1 > primeiraNota) {
			primeiraNota = alunos[i].n1;
		}

		if ((alunos[i].n1 + alunos[i].n2 + alunos[i].n3)/3 > mediaGeral) {
			mediaGeral = (alunos[i].n1 + alunos[i].n2 + alunos[i].n3)/3;
		}
	}

	printf("\n(b) Aluno com maior primeira nota: \n");
	


	return 0;
}