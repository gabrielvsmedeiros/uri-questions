#include <stdio.h>
#include <stdlib.h>

int main() {
	// Estrutura do tipo Registro que possui dois campos do tipo ponteiro para inteiro
	typedef struct {
		int *ptr1;
		int *ptr2;
	} Registro;

	int i1 = 100;

	// Criando uma variável do tipo Registro e outro do tipo ponteiro para Registro
	Registro reg, *reg_ptr;

	reg.ptr1  = &i1;
	reg_ptr   = &reg;

	*reg.ptr1 = 35;

	// As próximas duas linhas são iguais
	*(*reg_ptr).ptr1 = *reg.ptr1 / 7;
	*reg_ptr->ptr1   = *reg_ptr->ptr1 + 5;

	printf("%i, %i, %i, %i\n", i1, *reg.ptr1, *(*reg_ptr).ptr1, *reg_ptr->ptr1 );

	return 0;
}