#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int codigo;
    char descricao[50];
    float valor;
    int estoque;
} Produto;

typedef struct {
    char nome[50];
    Produto produtos[100];
} Loja;

int escolheOpcao();
void imprimeMenu();
void leituraLoja(Loja *loja);
void leituraProduto(Produto *produto);
void insereLoja(Loja *atacado, Loja loja, int *qtdLojas);
void imprimeLojas(Loja *atacado, int qtdLojas);
void insereProduto(char *loja, Produto produto, Loja *atacado);
void precoAbaixoDaMedia(Loja *atacado, int codigo, int qtdLojas);

int main(int argc, char const *argv[]) {
	int n, qtdLojas = 0, codigo;
	char lojaAddProduto[50], opcao;
  // CRIAÇÃO DE UM CONJUNTO DE LOJAS (ATACADO) E DE UMA LOJA ESPECÍFICA
  // ZERANDO RESPECTIVAS VARIÁVEIS PARA A LÓGICA DE IMPRESSÃO
	Loja *atacado = {0}, loja = {0};
	Produto produto = {0};
  // LEITURA DO TAMANHO DA LISTA DE LOJAS
	printf("ENTRE COM O NÚMERO MÁXIMO DE LOJAS: \n");
	scanf("%d", &n);
  // ALOCAÇÃO DINÂMICA DA LISTA DE LOJAS
  atacado = (Loja *)calloc(n,sizeof(Loja));

  do {
    // FUNÇÃO QUE "ZERA" TODOS OS ELEMENTOS DESSA STRING
    memset(lojaAddProduto,(char)0,sizeof(char)*50);
    imprimeMenu();
		opcao = escolheOpcao();
		if (opcao == '1') {// Inserir uma nova loja
			leituraLoja(&loja);
			insereLoja(atacado, loja, &qtdLojas);
		}
    else if (opcao == '2') {// Inserir um novo produto em uma loja (a loja é dada pelo usuário e precisa ter sido cadastrada previamente)
			printf("\nDigite o nome de uma Loja: ");
      scanf("%s", lojaAddProduto);
			leituraProduto(&produto);
      insereProduto(lojaAddProduto, produto, atacado);
		}
    else if (opcao == '3') {// Imprimir todas as lojas e seus respectivos produtos (conforme exemplo abaixo)
			imprimeLojas(atacado, qtdLojas);
		}
    else if (opcao == '4') {// Listar quais lojas tem determinado produto com o valor abaixo da média das lojas (descrição abaixo no item "d)"
      printf("\nDigite o código do produto: ");
      scanf("%d", &codigo);
      precoAbaixoDaMedia(atacado, codigo, qtdLojas);
		}
    else {// Valor digitado não faz parte dos valores do menu
      printf("\nDigite um dos valores do MENU!!!\n");
    }
  } while (opcao != '5');

  return 0;
}

// FUNÇÃO QUE IMPRIME O MENU
void imprimeMenu() {
  printf("\n\t***MENU***\n\n");
  printf("1 - Inserir uma Loja\n");
  printf("2 - Inserir produto numa loja\n");
  printf("3 - Imprimir lojas e seus produtos\n");
  printf("4 - Lojas que possuem produtos com valor abaixo da média das lojas\n");
  printf("5 - Sair\n\n");
}

// FUNÇÃO QUE LÊ UMA DAS OPÇÕES DO MENU
int escolheOpcao() {
  char opt;
  scanf("%s", &opt);
  return opt;
}

// FUNÇÃO QUE FAZ A LEITURA DO NOME DA LOJA
void leituraLoja(Loja *loja) {
  printf("Nome da Loja (Digite o nome sem espaços): ");
  scanf("%s", loja->nome);
  return;
}

// FUNÇÃO QUE FAZ A LEITURA DE TODOS OS CAMPOS DO PRODUTO
void leituraProduto(Produto *produto) {
  printf("Codigo: ");
  scanf("%d", &produto->codigo);
  printf("Descrição: (Digite a Descrição sem espaços)");
  scanf("%s", produto->descricao);
  printf("Valor(R$): ");
  scanf("%f", &produto->valor);
  printf("Estoque: ");
  scanf("%d", &produto->estoque);
	return;
}

// FUNÇÃO QUE INSERE A NOVA LOJA NA LISTA DE LOJAS
void insereLoja(Loja *atacado, Loja loja, int *qtdLojas){
  atacado[*qtdLojas] = loja;
  (*qtdLojas)++;
  return;
}

// FUNÇÃO QUE IMPRIME TODAS AS LOJAS DO NOSSO ATACADO E SEUS RESPECTIVOS PRODUTOS
void imprimeLojas(Loja *atacado, int qtdLojas){
  int i, j;
  if (atacado[0].nome) {
    printf("\nVocê possui %d Lojas cadastradas: \n\n", qtdLojas);
    for (i = 0; i < qtdLojas; i++) {
      printf("\tLoja “%s” - produtos: \n", atacado[i].nome);
      for (j = 0; j < 100; j++) {
        if (atacado[i].produtos[j].codigo) {
          printf("\t\tCodigo: %d\n", atacado[i].produtos[j].codigo);
          printf("\t\tDescricao: %s\n", atacado[i].produtos[j].descricao);
          printf("\t\tValor: %.2f\n", atacado[i].produtos[j].valor);
          printf("\t\tEstoque: %d\n\n", atacado[i].produtos[j].estoque);
        }
      }
    }
  } else printf("Nenhuma loja cadastrada\n");
  return;
}

// FUNÇÃO QUE INSERE UM PRODUTO NA PRÓXIMA POSIÇÃO VAZIA DO VETOR PRODUTOS[] DENTRO DE UMA DETERMINADA L0JA
void insereProduto(char *loja, Produto produto, Loja *atacado) {
  int i, j;
  for (i = 0; i < 10; i++) {
    if ( strcmp( loja, atacado[i].nome ) == 0 ) {
      for (j = 0; j < 100; j++) {
        if (!(atacado[i].produtos[j].codigo)) {
          atacado[i].produtos[j].codigo = produto.codigo;
          strcpy(atacado[i].produtos[j].descricao, produto.descricao);
          atacado[i].produtos[j].valor = produto.valor;
          atacado[i].produtos[j].estoque = produto.estoque;
          break;
        }
        printf("\n\tProduto Inserido com sucesso!\n");
      }
    }
  }
	return;
}

// FUNÇÃO QUE IMPRIME AS LOJAS CUJO PREÇO DE DETERMINADO PRODUTO ESTÁ ABAIXO DA MÉDIA DOS PREÇOS DE TODAS AS LOJAS QUE POSSUEM ESTE PRODUTO EM ESTOQUE
void precoAbaixoDaMedia(Loja *atacado, int codigo, int qtdLojas) {
  int i, j, k = 0;
  float soma, media;
  // CALCULA A MEDIA DOS PRECOS DE TODOS OS PRODUTOS CUJO CODIGO É IGUAL AO PASSADO E POSSUI ESTOQUE
  for (i = 0; i < qtdLojas; i++) {
    for (j = 0; j < 100; j++) {
      if (atacado[i].produtos[j].codigo) {
        // VERIFICA SE O CÓDIGO DO PRODUTO DA VEZ É O MESMO QUE O CÓDIGO PASSADO PELO USER
        if (atacado[i].produtos[j].codigo == codigo) {
          // VERIFICA SE TEM DESTE PRODUTO NO ESTOQUE DA LOJA
          if (atacado[i].produtos[j].estoque > 0) {
            soma += atacado[i].produtos[j].valor;
            k++;
          }
        }
      } else {
        break;
      }
    }
  }
  media = soma/k;
  printf("\nLOJAS COM VALOR ABAIXO DA MEDIA\n");
  for (i = 0; i < qtdLojas; i++) {
    // PARA CADA LOJA TENHO QUE PERCORRER UM VETOR DE 0 - 100 PRODUTOS
    for (j = 0; j < 100; j++) {
      // VERIFICA A EXISTÊNCIA DE UM PRODUTO
      if (atacado[i].produtos[j].codigo) {
        // VERIFICA SE O CÓDIGO DO PRODUTO DA VEZ É O MESMO QUE O CÓDIGO PASSADO PELO USER
        if (atacado[i].produtos[j].codigo == codigo) {
          // VERIFICA SE O VALOR DESSE PRODUTO É MENOR DO QUE A MEDIA
          if (atacado[i].produtos[j].valor < media) {
            // VERIFICA SE TEM DESTE PRODUTO NO ESTOQUE DA LOJA
            if (atacado[i].produtos[j].estoque > 0) {
              printf("Loja %s!\n", atacado[i].nome);
            }
          }
        }
      } else {
        break;
      }
    }
  }
}
