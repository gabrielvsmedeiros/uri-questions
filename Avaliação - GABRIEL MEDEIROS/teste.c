#include <stdio.h>
#include <stdlib.h>

int menorDeDois(int a, int b) {
  return (a < b)? a : b;
}

int menorDeTres(int a, int b, int c) {
  return menorDeDois(menorDeDois(a, b), c);
}

int maior(int a, int b) {
  return (a > b)? a : b;
}

void diagonal(int **matriz, int l, int c) {
    int i, j;
    for (i = 1; i <= (l+c-1); i++) {
        int col_inicial = maior(0, i-l);
        int count = menorDeTres(i, (l-col_inicial), l);
        for (j = 0; j < count; j++)
            printf("%d ", matriz[menorDeDois(l, i)-j-1][col_inicial+j]);
    }
}

// void espiral(int **matriz, int linhas, int colunas) {
  int i;
  int k = 0, l = 0;

  /*
  * i - início das linhas
  * colunas - número de linhas
  * l - início das colunas
  * linhas - número de colunas
  * i - contador
  */

  while (k < linhas && l < n) {
    for (i = l; i < colunas; i++) {
      printf("%d ", a[k][i]);
    }
    k++;

    /* Print the last column from the remaining columns */
    for (i = k; i < linhas; i++) {
      printf("%d ", a[i][colunas-1]);
    }
    n--;

    /* Print the last row from the remaining rows */
    if ( k < linhas) {
      for (i = n-1; i >= l; i--) {
        printf("%d ", a[linhas-1][i]);
      }
      linhas--;
    }

    /* Print the first column from the remaining columns */
    if (l < colunas) {
      for (i = linhas-1; i >= k; i--) {
        printf("%d ", a[i][l]);
      }
      l++;
    }
  }
}

// Driver program to test above functions
int main() {
  int **matriz;
  int l, c, i, j;
  scanf("%d %d", &l, &c);

  // ALOCANDO MATRIZ
  matriz = (int **)calloc(l, sizeof(int*));
  if (matriz == NULL) return 1;
  for (i = 0; i < c; i++)	{
    matriz[i] = (int *)malloc(c*sizeof(int));
    if (matriz[i] == NULL) return 1;
  }

  // RECEBENDO VALORES
  for (i = 0; i < l; i++) {
    for (j = 0; j < c; j++) scanf("%d", &matriz[i][j]);
  }

  printf("\nPERCURSO EM DIAGONAL\n");
  diagonal(matriz, l, c);
  printf("\n");
  printf("\nPERCURSO EM ESPIRAL\n");
  espiral(matriz, l, c);
  printf("\n");

  return 0;
}
