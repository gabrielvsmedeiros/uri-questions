#include <stdio.h>
#include <stdlib.h>

void diagonal(int **matriz, int l, int c) {
	int count = 0, i, j;
	do {
		for (i = l-1; i >= 0; i--) {
			for (j = 0; j < c; j++) {
				if ((i+j) == count) {
					printf("%d ", matriz[i][j]);
				}
			}
		}
		count++;
	} while (count < (l*c));
	return;
}

void espiral(int **matriz, int l, int c) {
	int i = 0, j = 0, count = 0;
	do {
		for (j = 0; j < c; j++) {
			printf("%d ", matriz[i][j]);
		}
		i++;
	} while(count < (l*c));
}

int main() {
	int **matriz;
	int m, n, i, j;
	scanf("%d %d", &m, &n);

	// ALOCANDO MATRIZ
	matriz = (int **)calloc(m, sizeof(int*));
	if (matriz == NULL) return 1;
	for (i = 0; i < n; i++)	{
		matriz[i] = (int *)malloc(n*sizeof(int));
		if (matriz[i] == NULL) return 1;
	}

	// RECEBENDO VALORES
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) scanf("%d", &matriz[i][j]);
	}

	printf("\nPERCURSO EM ESPIRAL\n");
	espiral(matriz, m, n);

	printf("\nPERCURSO EM DIAGONAL\n");
	diagonal(matriz, m, n);

	// DESALOCANDO MATRIZ
	for(i = 0; i < m; i++) free(matriz[i]);
	free(matriz);

	return 0;
}
