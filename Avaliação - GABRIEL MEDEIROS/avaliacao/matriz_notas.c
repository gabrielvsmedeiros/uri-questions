#include <stdio.h>
#include <stdlib.h>

float media(float **turma, int i, int n) {
	if (n == 3) return 0.0;
	else return (turma[i][n]/3)+media(turma, i, n+1);
}

int main() {
	float **turma;
	int m, n = 0, i, j;
	scanf("%d", &m);

	// ALOCANDO MATRIZ
	turma = (float **)calloc(m, sizeof(float*));
	if (turma == NULL) return 1;
	for (i = 0; i < 4; i++)	{
		turma[i] = (float *)malloc(4*sizeof(float));
		if (turma[i] == NULL) return 1;
	}

	// RECEBENDO VALORES
	for (i = 0; i < m; i++) {
		for (j = 0; j < 3; j++) scanf("%f", &turma[i][j]);
		turma[i][3] = media(turma, i, n);
	}

	// IMPRIMINDO VALORES
	printf("\n n1  - n2  - n3  - media\n");
	for (i = 0; i < m; i++) {
		for (j = 0; j < 4; j++) printf("%.2f ", turma[i][j]);
		printf("\n");
	}

	// DESALOCANDO MATRIZ
	for(i = 0; i < m; i++) free(turma[i]);
	free(turma);

	return 0;
}
