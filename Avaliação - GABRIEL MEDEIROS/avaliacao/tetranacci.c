#include <stdio.h>

int tetranacci(int n) {
  if (n == 1 || n == 2 || n == 3) return 0;
  else if (n == 4) return 1;
	else return tetranacci(n-1) + tetranacci(n-2) + tetranacci(n-3) + tetranacci(n-4);
}

int main() {
  int n;
  scanf("%d", &n);
  printf("O enésimo número da sequência eh: %d\n", tetranacci(n+1));
return 0;
}
