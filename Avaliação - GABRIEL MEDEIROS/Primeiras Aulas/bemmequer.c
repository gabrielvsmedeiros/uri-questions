#include <stdio.h>

int main() {
  int petalas;
  char inicio;

  scanf("%d %c", &petalas, &inicio);

  if (petalas%2 == 0 && inicio == 'B' || petalas%2 == 1 && inicio == 'M') {
    printf("Love me not...\n");
  }

  if (petalas%2 == 0 && inicio == 'M' || petalas%2 == 1 && inicio == 'B') {
    printf("Love me...\n");
  }

  if (inicio != 'M' && inicio != 'B') { // invalid
    printf("Invalid Caracter\n");
  }

  return 0;
}
