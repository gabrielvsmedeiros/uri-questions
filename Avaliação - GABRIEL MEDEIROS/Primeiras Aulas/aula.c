#include <stdio.h>

int main(int argc, char const *argv[]) {
  int t, aux = 0;
  printf("Tamanho: ");
  scanf("%i", &t);

  int vetor[t], i;
  for (i = 0; i < t; i++) {
    printf("Digite um valor entre 1 e 50: ");
    scanf("%i", &vetor[i]);
     if (vetor[i] == 0) {
       printf("Você digitou um zero, camarada!\n");
       break;
     } else if (vetor[i] == t) {
       aux ++;
     }

     if (vetor[i] > 50 || vetor[i] < 1) {
       printf("Formato inválido\n");
       i--;
       continue;
     }
  }

  printf("O número %i foi digitado %i vezes.\n", t, aux);

  return 0;
}
