#include <stdio.h>

int main(int argc, char const *argv[])
{
	int n, i, atual=1, anterior=0, proximo=0;
	scanf("%d", &n);

	for(i=1;i<=n;i++){
		if(i<=1){
			proximo = i;
			break;
		}
		else{
			proximo = atual + anterior;
			anterior = atual;
			atual = proximo;
		}
	}
	printf("Fib(%d) = %d\n", n, proximo);

	return 0;
}