#include <stdio.h>

void main() {
  float x, y;

  scanf("%f %f", &x, &y);

  if (x < y && x > y/2) {
    printf("\nCondição Verdadeira!\n");
  } else {
    printf("\nCondição Falsa!\n");
  }

}
