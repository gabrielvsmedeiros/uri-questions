#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct End{
	char logradouro[31];
	int numero;
	char cep[9];
}Endereco;

struct Cadastro{
	char nome[51];
	Endereco endereco;
	char telefone[11];
};

void cadastra(struct Cadastro *c, int *pos){
	char nome[51];
	Endereco endereco;
	char telefone[11];

	printf("Nome: ");
	fgets(nome, 50, stdin);
	printf("Endereço: \n");
	printf("\tLogradouro: ");
	fgets(endereco.logradouro, 30, stdin);
	printf("\tNumero: ");
	scanf("%d", &endereco.numero);
	printf("\tCEP: ");
	scanf("%s", endereco.cep);
	printf("Telefone: ");
	scanf("%s", telefone);

	strcpy(c[*pos].nome, nome);
	c[*pos].endereco = endereco;
	strcpy(c[*pos].telefone, telefone);
	*pos = *pos + 1;
	return;
}

void imprimir(struct Cadastro *c, int pos){
	char nome[51]; 
	Endereco endereco;
	char telefone[11];
	struct Cadastro c_novo[50], tmp;

	int i, j, diff;
	
	for(i=0;i<pos;i++){
		c_novo[i] = c[i];
	};
	
	for(i=0;i<pos;i++){
		for(j=i;j<pos;j++){
			diff = strcmp(c_novo[i].nome, c_novo[j].nome);
			if(diff>0){
				tmp = c_novo[i];
				c_novo[i] = c_novo[j];
				c_novo[j] = tmp;
			}
		}
	}
	
	for(i=0;i<pos;i++){
		printf("\tREGISTRO #%d\n", i);
		printf("\t\tNome: %s\n", c_novo[i].nome);
		printf("\t\tEndereço:\n");
		printf("\t\t\tLogradouro: %s\n", c_novo[i].endereco.logradouro);
		printf("\t\t\tNumero: %d\n", c_novo[i].endereco.numero);
		printf("\t\t\tCEP: %s\n", c_novo[i].endereco.cep);
		printf("\t\tTelefone: %s\n", c_novo[i].telefone);
	}


	return;
}

int main(int argc, char const *argv[])
{
	int pos = 0, op;
	struct Cadastro c[50];

	do{
		printf("Digite uma opcao:\n");
		printf("1 - Novo cadastro\n");
		printf("2 - Imprimir em ordem alfabetica\n");
		printf("3 - Sair\n");
		scanf("%d", &op);
		getchar();

		if(op > 3 || op < 1){
			printf("Opção inválida\n");
			continue;
		}

		if(op == 1){
			
			cadastra(c, &pos);
		}
		if(op == 2){
			imprimir(c, pos);
		}

	}while(op != 3);
	return 0;
}