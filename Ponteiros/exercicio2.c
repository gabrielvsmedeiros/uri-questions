#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int* uniao(int *x1, int *x2, int n1, int n2, int *qtd) {
	int *x3, i, j;
	int n3 = n1 + n2; // TAMANHO DO VETOR 3 É A SOMA DOS TAMANHOS DOS OUTROS DOIS
	*qtd = n3;
	x3 = (int *)calloc(n3,sizeof(int)); // ALOCANDO O VETOR x3
	if (x3 == NULL) return NULL; // VERIFICA SE O ESPAÇO DE MEMÓRIA FOI ENCONTRADO
	
	for (i = 0; i < n1; i++) x3[i] = x1[i]; // ADICIONANDO OS VALORES DO PRIMEIRO VETOR AO TERCEIRO (x3) 	
	for (i = n1, j = 0; i < n3; i++, j++) x3[i] = x2[j]; // ADICIONANDO OS VALORES DO SEGUNDO VETOR AO TERCEIRO (x3)

	*qtd = n3;
	// printf("\nArray 3 (UNION)\n");
	// for (i = 0; i < n3; i++) printf("%i ", x3[i]); // IMPRIME OS VALORES DO VETOR x3
	// printf("\n");
	return x3;
}

int main() {
	int *vetor1, *vetor2, *vetor3, n1, n2, i, n3 = 0;
	scanf("%d %d", &n1, &n2); // LENDO QUANTIDADE DE ELEMENTOS DOS VETORES 1 E 2

	vetor1 = (int *)calloc(n1,sizeof(int)); // ALOCANDO O VETOR 1
	if(vetor1 == NULL) return 1; // VERIFICA SE O ESPAÇO DE MEMÓRIA FOI ENCONTRADO
	printf("The first array was alocaated in: %p;\n", vetor1);
		
	vetor2 = (int *)calloc(n2,sizeof(int)); // ALOCANDO O VETOR 2
	if(vetor2 == NULL) return 1; // VERIFICA SE O ESPAÇO DE MEMÓRIA FOI ENCONTRADO
	printf("The second array was alocaated in: %p;\n", vetor2);

	srand(time(0));
	for (i = 0; i < n1; i++) vetor1[i] = rand() % 10 + 1; // POPULANDO VETOR 1
	for (i = 0; i < n2; i++) vetor2[i] = rand() % 10 + 1; // POPULANDO VETOR 2

	printf("Array 1\n");
	for (i = 0; i < n1; i++) printf("%i ", vetor1[i]);
	printf("\nArray 2\n");
	for (i = 0; i < n2; i++) printf("%i ", vetor2[i]);

	printf("\nArray 3\n");
	vetor3 = uniao(vetor1, vetor2, n1, n2, &n3); // RETORNA O TERCEIRO VETOR (UNIÃO)
	for (i = 0; i < n3; i++) printf("%d ", vetor3[i]);

	printf("\nSize of array Union:  %i\n", n3);
	
	return 0;
}
