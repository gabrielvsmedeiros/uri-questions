#include "cadastro.h"
#include "entrada_saida.h"

void AdicionaPessoa(Pessoa *povo, Pessoa p, int *qtdPessoas){
    povo[*qtdPessoas] = p;
    (*qtdPessoas)++;

    return;
}

void adicionar(FILE *arquivo){
    int reg;
    char nome[51];
    Pessoa p;

    printf("\tEntre com o nome do novo arquivo a ser alterado: ");
    scanf("%s", nome);
    arquivo = fopen(nome, "a+");
    if(arquivo==NULL){
        printf("Erro ao abrir arquivo\n");
        return;
    }
    printf("\tEntre com o numero do registro a ser inserido:");
    scanf("%d", &reg);
    LePessoaDeTeclado(&p);

    fprintf(arquivo, "Posiçao %d:\n", reg);
    fprintf(arquivo, "\t\tNome: %s\n", p.nome);
    fprintf(arquivo, "\t\tAltura: %.2f\n", p.altura);
    fprintf(arquivo, "\t\tData de Nascimento: %02d/%02d/%04d\n", p.nascimento.dia, p.nascimento.mes, p.nascimento.ano);
    
    fclose(arquivo);
}

void gravarNovo(Pessoa *povo, int qtdPessoas, FILE *arquivo){
    int i;
    char nome[51];

    printf("Entre com o nome do novo arquivo a ser criado: ");
    scanf("%s", nome);
    arquivo = fopen(nome, "w");
    if(arquivo==NULL){
        printf("Erro ao criar arquivo\n");
        return;
    }
    for(i=0;i<qtdPessoas;i++){
        fprintf(arquivo, "Posiçao %d:\n", i);
        fprintf(arquivo, "\t\tNome: %s\n", povo[i].nome);
        fprintf(arquivo, "\t\tAltura: %.2f\n", povo[i].altura);
        fprintf(arquivo, "\t\tData de Nascimento: %02d/%02d/%04d\n", povo[i].nascimento.dia, povo[i].nascimento.mes, povo[i].nascimento.ano);
    }
    fclose(arquivo);
}

void recuperar(Pessoa *povo, int *qtdPessoas,FILE *arquivo){
    int pos=0;
    char nome[51], resposta;
    printf("ATENCAO: esta ação irá SOBREESCREVER os registros atualmente existentes na memória. Deseja realmente continuar (S/N)?");
    scanf(" %c", &resposta);
    if(tolower(resposta) == 'n'){
        return;
    }
    printf("Entre com o nome do novo arquivo a ser recuperado: ");
    scanf("%s", nome);
    arquivo = fopen(nome, "r");
    if(arquivo==NULL){
        printf("Erro ao abrir arquivo\n");
        return;
    }
    while(fscanf(arquivo, "Posiçao %d:\n", &pos)!=EOF){
        fscanf(arquivo, "\t\tNome: %s\n", povo[pos].nome);
        fscanf(arquivo, "\t\tAltura: %f\n", &povo[pos].altura);
        fscanf(arquivo, "\t\tData de Nascimento: %d/%d/%d\n", &povo[pos].nascimento.dia, &povo[pos].nascimento.mes, &povo[pos].nascimento.ano);
        
    }
    *qtdPessoas = pos+1;
    printf("\n\tTotal de %d registros lidos e adicionados com sucesso!\n\n", *qtdPessoas);
    fclose(arquivo);
}

