#include <stdio.h>
#include <stdlib.h>

int main(){
	int **matrizA, **matrizB, i, j, n;

	scanf("%d", &n);

	//alocando a matriz
	matrizA = (int**)calloc(n,sizeof(int*));
	if(matrizA == NULL)
		return 1;
	printf("alocando MatrizA no endereço: %u\n", (int)matrizA);
	
	
	for(i=0;i<n;i++){
		matrizA[i] = (int*)calloc(n,sizeof(int));
		if(matrizA[i] == NULL)
		return 1;
		printf("alocando a linha %d da MatrizA no endereço: %u\n", i, (int)matrizA[i]);
		//matrizA++;
		for(j=0;j<n;j++){
			printf("elemento MatrizA[%d][%d] no endereço: %u\n", i,j, (int)(matrizA[i]+j));
		}

	}

	matrizB = (int**)calloc(n,sizeof(int*));
	if(matrizB == NULL)
		return 1;
	printf("\talocando MatrizB no endereço: %u\n", (int)matrizB);

	for(i=0;i<n;i++){
		matrizB[i] = (int*)calloc(n,sizeof(int));
		if(matrizB[i] == NULL)
		return 1;
		printf("\talocando a linha %d da MatrizB no endereço: %u\n", i, (int)matrizB[i]);
		//matrizB++;
		for(j=0;j<n;j++){
			printf("\telemento MatrizB[%d][%d] no endereço: %u\n", i,j, (int)(matrizB[i]+j));
		}
	}

	//matrizA[0][2]=3;
	//matrizA[1][1]=7;
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			printf("%d ", matrizA[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			printf("%d ", matrizB[i][j]);
		}
		printf("\n");
	}
	
	for(i=0;i<n;i++){
		free(matrizA[i]);
		free(matrizB[i]);
	}
	printf("finalizou a liberação das linhas\n");
	free(matrizA);
	free(matrizB);
	return 0;
}