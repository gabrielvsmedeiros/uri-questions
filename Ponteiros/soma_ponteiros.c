#include <stdio.h>
#include <stdlib.h>
int main(){
	int *p=NULL;
	int i, n, soma = 0;
	
	scanf("%d", &n);

	p = (int *)malloc(n * sizeof(int)); //ignorar por enquanto
	printf("endereco = %p;\n", p);
	for(i=0;i<n;i++){
		scanf("%d", p);	
		soma = soma + *p;
		printf("valor = %d; endereco = %p; soma = %d\n", *p, p, soma);
		p++;
	}

	printf("Soma = %d\n", soma);

	return 0;
	
}