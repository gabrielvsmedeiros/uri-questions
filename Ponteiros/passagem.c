#include <stdio.h>

void misterio( int *p, int *q ){
   int r = *p;
   *p = *q;
   *q = r;
}
 
int main ( ){
   int i = 1;
   int j = 2;
   misterio( &i, &j );
   printf( "%i %i\n", i, j );
   return 0;
}
