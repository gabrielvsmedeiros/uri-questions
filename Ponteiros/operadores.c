#include <stdio.h>

int main(int argc, char const *argv[])
{
	int *elefantinho;
	int x=666;
	printf("valor = %d; endereco = %p\n", x, &x);
	scanf("%d", &x);
	printf("valor = %d; endereco = %p\n", x, &x);

	printf("valor = %p; endereco = %p\n", elefantinho, &elefantinho);
	elefantinho = &x;
	elefantinho++;
	printf("valor = %p; endereco = %p\n", elefantinho, &elefantinho);
	return 0;
}