#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	int i, j, a, n;
	double **matriz, soma_linha = 0.0;
	
	scanf("%i %i", &a, &n);

	// CÓDIGO DE ALOCAÇÃO DO VETOR
	matriz = (double**)calloc(a, sizeof(double*));
	if (matriz == NULL) return 1;
	
	for (i = 0; i < a; i++) {
		matriz[i] = (double*)calloc(n+1, sizeof(double));
		if (matriz[i] == NULL) return 1;
	}

	// POPULANDO MATRIZ COM ALUNOS E NOTAS
	for (i = 0; i < a; i++) {
		for (j = 0; j < n+1; j++) {
			if (j == n) matriz[i][j] = soma_linha/n; // ÚLTIMA COLUNA -> RECEBE A MÉDIA DE NOTAS DA LINHA
			else {
				scanf("%lf", &matriz[i][j]); // LENDO AS NOTAS ENQUANTO NÃO FOR A ÚLTIMA COLUNA
				soma_linha += matriz[i][j]; // ATRIBUINDO O VALORES LIDOS ATÉ ÚLTIMA COLUNA DA LINHA
			}
		}
		soma_linha = 0; // ZERANDO VARIÁVEL QUE SOMA OS VALORES DE CADA LINHA
	}

	// IMPRIMINDO MATRIZ FINAL
	printf("\nFinal Matrix (the last column is the average of the grades that precede it): \n");
	for (i = 0; i < a; i++) {
		for (j = 0; j < n+1; j++) printf("%.2lf ", matriz[i][j]);
		printf("\n");
	}

	return 0;
}
