#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

typedef struct {
    int dia, mes, ano;
} Data;


typedef struct {
    char nome[30];
    float altura ;
    Data nascimento;
} Pessoa;


void ImprimeTelaDeOpcoes(){
    printf("1 - Inserir um nome\n");
    printf("2 - Listar nomes e alturas\n");
    printf("3 - Listar pessoas mais velhas\n");
    printf("4 - Gravar em novo arquivo\n");
    printf("5 - Adicionar em arquivo existente\n");
    printf("6 - Recuperar dados de um arquivo\n");
    printf("7 - Sair\n");

}

int EscolheOpcao(){
    int r;
    scanf("%d", &r);
    return r;
}

void LePessoaDeTeclado(Pessoa *p){
    printf("Entre com o nome: ");
    scanf("%s", p->nome);
    printf("Entre com a altura: ");
    scanf("%f", &p->altura);
    printf("Entre com a data de nascimento (DD MM AAAA): ");
    scanf("%d %d %d", &p->nascimento.dia, &p->nascimento.mes, &p->nascimento.ano);
    return;
}

void AdicionaPessoa(Pessoa *povo, Pessoa p, int *qtdPessoas){
    povo[*qtdPessoas] = p;
    (*qtdPessoas)++;

    return;
}

void ImprimeTodasAsPessoas(Pessoa *povo, int qtdPessoas){
    int i;
    printf("Imprimindo todas as %d pessoas: \n", qtdPessoas);
    for(i=0;i<qtdPessoas;i++){
        printf("Posiçao %d:\n", i);
        printf("\t\tNome: %s\n", povo[i].nome);
        printf("\t\tAltura: %.2f\n", povo[i].altura);
        printf("\t\tData de Nascimento: %02d/%02d/%04d\n", povo[i].nascimento.dia, povo[i].nascimento.mes, povo[i].nascimento.ano);
    }
}

void LeDataDeTeclado(Data *d){
    printf("Entre com uma data (DD MM AAAA): ");
    scanf("%d %d %d", &d->dia, &d->mes, &d->ano);
    return;
}

void ImprimeMaisVelhos(Pessoa *povo, int qtdPessoas, Data d){
    int i, maisVelho=0;
    printf("Lista das Pessoas mais velhas que a data %d/%02d/%d\n", d.dia, d.mes, d.ano);
    for(i=0;i<qtdPessoas;i++){
        maisVelho = 0;
        if(povo[i].nascimento.ano<d.ano){//eh mais velho ou igual
            maisVelho = 1;
        }
        else{
            if(povo[i].nascimento.ano==d.ano && povo[i].nascimento.mes<d.mes){
                maisVelho = 1;
            }
            else{
                if(povo[i].nascimento.mes==d.mes && povo[i].nascimento.dia<d.dia){    
                    maisVelho = 1;
                }
            }
        }
        if(maisVelho){
            printf("Posiçao %d:\n", i);
            printf("\t\tNome: %s\n", povo[i].nome);
            printf("\t\tAltura: %.2f\n", povo[i].altura);
            printf("\t\tData de Nascimento: %02d/%02d/%04d\n", povo[i].nascimento.dia, povo[i].nascimento.mes, povo[i].nascimento.ano);
        }
    }

}

void gravarNovo(Pessoa *povo, int qtdPessoas, FILE *arquivo){
    int i;
    char nome[51];

    printf("Entre com o nome do novo arquivo a ser criado: ");
    scanf("%s", nome);
    arquivo = fopen(nome, "w");
    if(arquivo==NULL){
        printf("Erro ao criar arquivo\n");
        return;
    }
    for(i=0;i<qtdPessoas;i++){
        fprintf(arquivo, "Posiçao %d:\n", i);
        fprintf(arquivo, "\t\tNome: %s\n", povo[i].nome);
        fprintf(arquivo, "\t\tAltura: %.2f\n", povo[i].altura);
        fprintf(arquivo, "\t\tData de Nascimento: %02d/%02d/%04d\n", povo[i].nascimento.dia, povo[i].nascimento.mes, povo[i].nascimento.ano);
    }
    fclose(arquivo);
}

void adicionar(FILE *arquivo){
    int reg;
    char nome[51];
    Pessoa p;

    printf("\tEntre com o nome do novo arquivo a ser alterado: ");
    scanf("%s", nome);
    arquivo = fopen(nome, "a+");
    if(arquivo==NULL){
        printf("Erro ao abrir arquivo\n");
        return;
    }
    printf("\tEntre com o numero do registro a ser inserido:");
    scanf("%d", &reg);
    LePessoaDeTeclado(&p);

    fprintf(arquivo, "Posiçao %d:\n", reg);
    fprintf(arquivo, "\t\tNome: %s\n", p.nome);
    fprintf(arquivo, "\t\tAltura: %.2f\n", p.altura);
    fprintf(arquivo, "\t\tData de Nascimento: %02d/%02d/%04d\n", p.nascimento.dia, p.nascimento.mes, p.nascimento.ano);
    
    fclose(arquivo);
}

void recuperar(Pessoa *povo, int *qtdPessoas,FILE *arquivo){
    int pos=0;
    char nome[51], resposta;
    printf("ATENCAO: esta ação irá SOBREESCREVER os registros atualmente existentes na memória. Deseja realmente continuar (S/N)?");
    scanf(" %c", &resposta);
    if(tolower(resposta) == 'n'){
        return;
    }
    printf("Entre com o nome do novo arquivo a ser recuperado: ");
    scanf("%s", nome);
    arquivo = fopen(nome, "r");
    if(arquivo==NULL){
        printf("Erro ao abrir arquivo\n");
        return;
    }
    while(fscanf(arquivo, "Posiçao %d:\n", &pos)!=EOF){
        fscanf(arquivo, "\t\tNome: %s\n", povo[pos].nome);
        fscanf(arquivo, "\t\tAltura: %f\n", &povo[pos].altura);
        fscanf(arquivo, "\t\tData de Nascimento: %d/%d/%d\n", &povo[pos].nascimento.dia, &povo[pos].nascimento.mes, &povo[pos].nascimento.ano);
        
    }
    *qtdPessoas = pos+1;
    printf("\n\tTotal de %d registros lidos e adicionados com sucesso!\n\n", *qtdPessoas);
    fclose(arquivo);
}

int main()
{
    Pessoa povo[10];
    Pessoa p;
    int opcao, qtdPessoas = 0 ;
    Data dia;
    FILE *arquivo;

    
    do
    {
      ImprimeTelaDeOpcoes();
      opcao = EscolheOpcao();
     if (opcao == 1)
     {
        LePessoaDeTeclado(&p);
        AdicionaPessoa(povo, p, &qtdPessoas);
     }
     if (opcao == 2)
     {
        ImprimeTodasAsPessoas(povo, qtdPessoas);
     }
     if (opcao == 3)
     {
        LeDataDeTeclado(&dia);
        ImprimeMaisVelhos(povo, qtdPessoas, dia);
     } 
     if(opcao == 4){
        gravarNovo(povo, qtdPessoas, arquivo);
     }
     if(opcao == 5){
        adicionar(arquivo);
     }
     if(opcao == 6){
        recuperar(povo, &qtdPessoas, arquivo);
     }
    } while (opcao != 7);

    return 0;
}